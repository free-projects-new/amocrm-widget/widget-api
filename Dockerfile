FROM python:3.11

ENV PYTHONUNBUFFERED 1

ENV PROJECT_PATH /app

WORKDIR $PROJECT_PATH

# Copy requirements for catch
ADD ./requirements.txt $PROJECT_PATH
RUN pip3 install -r requirements.txt

# Copy project files
ADD . $PROJECT_PATH
