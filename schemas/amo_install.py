from pydantic import BaseModel, Field
from datetime import datetime


class AmoInstallBase(BaseModel):
    code: str | None
    last_code: int | None
    referrer: str | None
    platform: int | None
    amo_account_id: int | None
    client_id: str | None
    client_secret: str | None
    from_widget: int | None
    refresh_token: str | None
    access_token: str | None
    pair_token: str | None
    expires_in: int | None


class AmoInstallDetail(AmoInstallBase):
    id: int = Field(primary_key=True)
    created_at: datetime | None
    updated_at: datetime | None
    active: bool | None

    class Config:
        orm_mode = True
