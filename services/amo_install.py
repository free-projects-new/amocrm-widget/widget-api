import datetime

from sqlalchemy.orm import Session

from models.amo_install import AmoInstall
from schemas.amo_install import AmoInstallBase


def create_amo_user(db: Session, data: AmoInstallBase):
    amo_install = AmoInstall(**data.dict())
    amo_install.created_at = datetime.datetime.now()
    amo_install.updated_at = datetime.datetime.now()
    amo_install.active = True
    db.add(amo_install)
    db.commit()
    db.refresh(amo_install)
    return amo_install

