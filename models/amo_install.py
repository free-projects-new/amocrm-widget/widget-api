from sqlalchemy import Column, String, Integer, BigInteger, Boolean, DateTime
from core.db import Base


class AmoInstall(Base):
    __tablename__ = 'amo_install'

    id = Column(BigInteger, autoincrement=True, primary_key=True, index=True, unique=True)
    code = Column(String, nullable=True)
    last_code = Column(Integer, nullable=True)
    referrer = Column(String, nullable=True)
    platform = Column(Integer, nullable=True)
    amo_account_id = Column(Integer, nullable=True, index=True)
    client_id = Column(String, nullable=True, index=True)
    client_secret = Column(String, nullable=True)
    from_widget = Column(Integer, nullable=True)
    refresh_token = Column(String, nullable=True)
    access_token = Column(String, nullable=True, index=True)
    pair_token = Column(String, nullable=True)
    expires_in = Column(Integer, nullable=True)
    active = Column(Boolean, default=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
