project-up:
	docker-compose -p amo-widget down
	docker-compose -p amo-widget up -d
project-down:
	docker-compose -p amo-widget down
project-migrate:
	docker-compose --project-name amo-widget --env-file .env run --rm app-cli alembic upgrade head