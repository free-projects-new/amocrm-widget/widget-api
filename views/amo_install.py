from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from schemas.amo_install import AmoInstallDetail, AmoInstallBase
from services.amo_install import create_amo_user
from utils.db import get_db

router = APIRouter()


@router.post('/install', response_model=AmoInstallDetail)
def install(db: Session = Depends(get_db), data: AmoInstallBase = AmoInstallBase()):
    return create_amo_user(db=db, data=data)
