from fastapi import FastAPI

from routes import root_api


app = FastAPI()

app.include_router(root_api)
