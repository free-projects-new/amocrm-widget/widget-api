from fastapi import APIRouter

from views.amo_install import router


root_api = APIRouter(prefix="/api/v1")


root_api.include_router(router)
